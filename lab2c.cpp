﻿﻿// lab2c.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
#include <iostream>
using namespace std;
// манипулятор со ссылкой на поток 
ostream& printTest(ostream& stream) {
    return stream << "Взлом пентагона завершен";
};

int main()
{
    setlocale(LC_ALL, "rus");
    cout << "Выполнить перевод (1 - С > F, 0 - F > C)";
    int a;
    cin >> a;
    if (a == 1)
    {
        int n, f;
        cout.width(10);
        cout << "Ввести число градусов\n";
        cin >> n;
        f = (n * 9 / 5) + 32;
        cout.precision(4);
        cout << hex << f << " фаренгейт\n" << endl;
        cout << dec << f << " фаренгейт\n" << endl;
        cout << oct << f << " фаренгейт\n" << endl;
        cout << "Flags:" << cout.flags() << endl;
        cout << "Манипулятор своего текста:" << printTest << endl;
        return 0;
    }
    if (a == 0)
    {
        int n, f;
        cout.width(10);
        cout << "Ввести число Фаренгейт\n";
        cin >> f;
        n = (f - 32) * 5 / 9;
        cout.precision(4);
        cout << right << hex << n << " градусов\n" << endl;
        cout << left << dec << n << " градусов\n" << endl;
        cout << right << oct << n << " градусов\n" << endl;
        cout << "Flags:" << cout.flags() << endl;
        cout << "Манипулятор своего текста:" << printTest << endl;
        return 0;
    }
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"


