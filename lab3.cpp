﻿#include <string>
#include <iostream>
#include <fstream>
using namespace std;
int main()
{
    setlocale(LC_ALL, "rus");
    // Размер буфера 
    const int buf_size = 15638;

    // Имя исходного файла
    string src_file = "TextFile.txt";
    string out_file = "TextFile_copy.txt";

    // Создание потока для работы с исходным файлом
    ifstream ifs(src_file.c_str(), std::ios::binary);

    // Подсчет размер исходного файла
    ifs.seekg(0, std::ios::end);
    ios::pos_type src_size = ifs.tellg();
    ifs.seekg(0, std::ios::beg);

    // Количество целых проходов (для частей файлы, которые полностью умещаются в буфер)
    size_t a_mount = src_size / buf_size;
    // Остаток (остаток файла)
    size_t b_mount = src_size % buf_size;
    cout << "количество символов: " << a_mount + b_mount;

    // Создание потока для файла-копии
    ofstream ofs(out_file.c_str(), std::ios::binary);

    // Это буфер
    char buf[buf_size];

    // Цикл по числу полных проходов
    for (size_t i = 0; i < a_mount; ++i)
    {
        ifs.read(buf, buf_size);
        ofs.write(buf, buf_size);
    }

    // Если есть остаток
    if (b_mount != 0)
    {
        ifs.read(buf, b_mount);
        ofs.write(buf, b_mount);
    }

    ifs.close();
    ofs.close();

    return 0;
}
